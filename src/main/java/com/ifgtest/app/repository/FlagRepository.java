package com.ifgtest.app.repository;

import com.ifgtest.app.model.Flag;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class FlagRepository implements PanacheRepository<Flag> {

}
