package com.ifgtest.app.consumer;

import org.eclipse.microprofile.reactive.messaging.Incoming;

import com.ifgtest.app.repository.FlagRepository;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class KafkaConsumer {
    @Inject
    FlagRepository flagRepository;

    @Incoming("employee-events")
    public void consumeEvent(Boolean event) {
        System.out.println("RECEIVED MESSAGE");
        var data = flagRepository.findById(1L);
        data.setIsAuthenticated(event);
        data.persist();
        System.out.println("Received event: " + event);
    }
}
