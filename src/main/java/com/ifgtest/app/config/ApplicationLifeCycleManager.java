package com.ifgtest.app.config;

import com.ifgtest.app.service.DummyDataService;

import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.transaction.Transactional;

@Singleton
public class ApplicationLifeCycleManager {
    @Inject
    DummyDataService dummyDataService;

    @Transactional
    void onStart(@Observes StartupEvent ev) {
        dummyDataService.populateDummyData();
    }

}
