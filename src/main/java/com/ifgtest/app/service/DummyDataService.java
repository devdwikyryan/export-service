package com.ifgtest.app.service;

import com.ifgtest.app.model.Employee;
import com.ifgtest.app.model.Flag;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;

@ApplicationScoped
public class DummyDataService {

    @Transactional
    public void populateDummyData() {
        Employee employee1 = new Employee("Ryan Dwiky", 32, "Engineer");
        Employee employee2 = new Employee("Agnes Rahajeng", 36, "Finance");
        Employee employee3 = new Employee("Abdul Harris", 27, "HR");
        Employee employee4 = new Employee("Bob Bill", 45, "Management");
        Employee employee5 = new Employee("Bill Ahmad", 60, "Owner");
        employee1.persist();
        employee2.persist();
        employee3.persist();
        employee4.persist();
        employee5.persist();

        Flag notYetAuthenticated = new Flag(false);
        notYetAuthenticated.persist();
    }

}
