package com.ifgtest.app;

import java.util.List;

import org.eclipse.microprofile.reactive.messaging.Incoming;

import com.ifgtest.app.model.Employee;
import com.ifgtest.app.model.Flag;
import com.ifgtest.app.repository.EmployeeRepository;
import com.ifgtest.app.repository.FlagRepository;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/")
public class ExportResource {

    @Inject
    FlagRepository flagRepository;

    @Inject
    EmployeeRepository employeeRepository;

    @GET
    @Path("/employee-list")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> getEmployeeList() {
        System.out.println("request incoming");
        return employeeRepository.listAll();
    }

    @GET
    @Path("/get-flag")
    @Produces(MediaType.APPLICATION_JSON)
    public Flag getFlag() {
        return flagRepository.findById(1L);
    }

    // @Incoming("employee-events")
    // public void consumeEvent(Boolean event) {
    // var data = flagRepository.findById(1L);
    // data.setIsAuthenticated(event);
    // data.persist();
    // System.out.println("Received event: " + event);
    // }
}
